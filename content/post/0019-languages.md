---
title: "Languages"
date: "2018-11-16"
draft: false
toc: false
comments: true
categories:
- Brainstorming
tags:
- Linguists Guide
- Language
---

I've done a bit more tinking and mapped out a few more languages- specifically, the languages of The Undercavern. While some of the races and languages listed here aren't currently found in any published material, I felt it was more important to take them into account now.

<!--more-->

With any luck, maybe I'll even get to a few adventures about these guys.

<span class="languagefamily"> [![Language Family](/languagefamily2.png)](/languagefamily2.png)</span>

OH, I've decided to start with Gnollish first since it's a relatively simple language. Features currently will include the following:

* Nomadic
* Dominant linguistic gender is feminine
* family/pack oriented
* More words for locations, directions, gathering, hunting
* Less words for possessions, farming 
* Expresses thoughts through power balances (lead/follow, resist/submit, challenge/support)
* Translations sound rude/accusatory to non-speakes
* Numeric system is base 8

I've gathered up my gnollish words and began breaking down the lexicon.



