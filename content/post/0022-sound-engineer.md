
---
title: "Seeking Sound Design partner for a Fantasy Horror podcast"
date: "2023-10-03"
draft: false
toc: false
comments: true
categories:
- News
- Expedition
tags:
- Podcast
---

After taking a break on the podcast for a few years (it's been 4 years already?!), I'm looking for a partner to help with it.

<!--more-->

Specifically, a Sound Designer.

What does that look like? I'm not sure. It'll probably be a heavy lift, truth be told.  It's a Horror/Fantasy that takes place in a cave system. There will be strange creatures, environmental effects, etc.

At this time, it's only a pipedream, so there's not a lot of money in it. However I did say I was looking for a partner, so I'd be willing to split any ad revenue with you.

Sound interesting? Let me know.