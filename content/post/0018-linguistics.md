---
title: "Linguistics"
date: "2018-11-10"
draft: false
toc: false
comments: true
categories:
- Brainstorming
tags:
- Linguists Guide
- Language
---

Things have slowed down a bit on the *Biographer's Guide* front, so my mind has been wondering as of late. As some of you may know, I listen to a lot of podcasts- mainly serialized fiction of various sorts (The Lift, Wormwood, EOS 10, Wolf 359, etc), and I keep playing with the idea of creating my own based on Faldin Stormbread's expedition into the Undercavern. Sadly I'll never have the time for it, but it is a fun mental exercise.

<!--more-->

This led me to thinking about voices and linguistics, and how each of the species would sound. A majority of the sound would be determined by physiological traits- For example, gnomes are going to have higher pitched voices than ogres due to their size, while dragonborn and kobolds are going to have more of a hiss due to their narrow snouts and tongues.

As I'm driving to work, I'll toy with different voices, and ponder accents. Most fantasy portrays dwarves as scottish-sounding, but I'd like them to have their own feel- I imagine the dwarven language to be very blocky and rough, almost like klingon. Speech should come from the back of the throat, and should sound have an almost russian sound to it.

Sure enough, this week I came across [this post](https://imgur.com/gallery/IbPSOa9) on Imgur showing where different sounds are produced in the mouth, which is awesome. I posted as much as was directed to [The Language Construction Kit](https://www.zompist.com/kit.html), which explains how to construct your own language.

Which leads me to this post.

So if I were to theoretically construct a language for each race, what would that look like? Granted, not all races would have their own languages- more likely they'd have dialects of more broad languages. The first step the is to determine how many languages I need, as well as dialects of those languages. 

I can simplify a great deal of this by having a single root language with several child languages. Per elven creation mythology, all the races were created by a godlike race called the Vorn. From their language, several different populations evolved their own languages, with influences from other external languages (such as draconic, angellic, and Demonic).

From Vornish, there were 7 known language groups- Gnollish, Blund, Scalene, Humanic, and Broden.

Over time, dialects formed which became new languages: Blund branched out into Orcine and Goblish, while Humanic branched into Elven and Dwarven branched into Minos. 

Vornish, however, isn't the only root language- there is also Demonic, Angelic, and Hausa (the language of magic). These three languages predate the vornish and are nearly incomprehensible to most people. Hausa was the only one of the three to directly branch into another language- Draconic. 

In addition, two more tertiary languages appeared- Kortino, which is a pidgin mix of Humanic and Broden, and Draconid, which is a blend of Scalene and Draconic.

The three ancient native languages have influenced other languages: Demonic influenced both Orcine and Scalene, Hausa influenced Elven, and Angellic influenced Kortino.

Here's a map as I currently envision it.
<span class="languagefamily"> ![Language Family](/languagefamily.png)</span>

So where does this leave us? I think I need to flesh out Vornish first, then perhaps develop Broden, Humanic, and Brund together. The real important part is that I'm going to have to add a Linguists Guide to my lineup.
