---
title: "Saying Goodbye to Google Plus"
date: 2018-10-14T22:15:52Z
draft: false
toc: false
comments: true
categories:
- News
tags:
- GooglePlus
- Facebook
- MeWe
---

As some of you know, I switched over to G+ a few years back after growing concerned with Facebook's direction. I drastically stopped posting on Facebook and removed most of the people I was connected to. What made G+ better than Facebook? It wasn't about who you know, but what what you knew.

Most interactions were based on collections or circles around specific topics. If you like to talk about potted plants, you could post to your potted plant collection which people for follow or unfollow as they wished. This drastically reduced much of the political arguing and let people focus on what they could agree on.

Over the past few years I made a ton of new friends in the Tabletop RPG community and even started publishing a few supplements myself on DriveThruRPG. I have friends that I've made there that I've been playing D&D with for a decade now.

Last week google announced it was closing G+. I wasn't surprised---the writing had been on the wall for some time---but I am sad to see it go. The thought of losing all of those friends (many of whom despise everything Facebook has become in that time) made me sad.

<!--more-->

Fortunately, something amazing happened. A large chunk of those gamers up and migrated to a new platform- MeWe.com. Within the first day I had filled my allotted 50 friend invites- I currently have 75 contacts active, with another dozen waiting for a response. For context, my high on Facebook was 400+, and G+ is currently at around 77 that I follow and ~780 that are following me.

Despite that low number, my MeWe feed has been blowing up with new content and posts- unlike Facebook, they keep things in chronological order and don't hide posts from your contacts to extort money out of you. I've been posting more to MeWe in the past two days than I had to G+ in the past 6 months.

It's not perfect- there's a large, pre-existing contingent of political trolls and conspiracy nuts that take advantage of their strict anti-censorship stance. It's also missing a lot of features that I really liked on G+- the groups are much less functional than communities, hashtags are lame compared to Collections, and Pages currently don't exist.

Since they don't run ads, they rely on a freemium model to make money. I'll probably end up spending some cash there because the service is valuable to me. But the thing that really makes me happy is that the developers are openly communicating with the G+ refugees and making changes/improvements that we're suggesting, showing a level of support that I've not seen on either G+ or Facebook. If it can keep up this momentum, I'll be a very happy camper.

My Facebook utilization probably won't grow that much now that G+ is gone, so if you want to see what I'm up to, you can follow me on MeWe: http://mewe.com/i/morgajel

While MeWe doesn't currently support Brand Pages (What Ronden Marr would be), their devs say it's already in the pipeline.

