---
title: "Date Formatting"
date: "2018-08-26"
draft: false
toc: false
comments: true
categories:
- News
tags:
- Biographers Guide
- Indiegogo
---
Here's a conundrum I'm currently facing- how to display dates. Specifically, birth and death years.

<!--more-->

The real problem arises when parsing out the indices. Lifespans can consist of one of the following formats:

- *100 A.C.*            Year born
- *Unknown*             Year born unknown

- *10 A.C. to 120 A.C.* Year born and died
- *100 A.C. to Unknown* Year born and presumed dead

I was finally able to make this work, but it took some effort. Now all characters are proper indexed by year of birth/and/or death.
