---
title: "Going on an Adventure"
date: 2019-04-17T16:26:03-04:00
draft: false
toc: false
comments: false
categories:
- News
- Expedition
tags:
- Podcast
---

I've been pretty scatterbrained lately, and I've had this idea for a podcast kicking around in my head for months. A few months ago, I decided to see how far I could get with the idea before I lost momentum. The result is a plot for a multi-season adventure with 10 episodes each season.

<!--more-->

Keep in mind, I've not recorded anything- I've just been writing. If I can get a rough draft of all 10 episodes of the first season done, I'll move ahead with recording it. In the meantime, I've added a placeholder in the menubar up top. Keep an eye out for future updates.
