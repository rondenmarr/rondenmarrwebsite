---
title: "Looking for Reviewers"
date: 2019-06-09T09:57:12-04:00
draft: false
toc: false
comments: true
categories:
- News
- Expedition
tags:
- Podcast
---

A quick update for the podcast is in order.

<!--more-->

I have a 6 episodes worth of rough drafts complete (some are more rough than others). I've reached the point where I'm comfortable letting others read the first episode. If you're interested in a sneak peek and providing feedback, please let me know.


