---
title: "Controlling the Horizontal and Vertical"
date: "2018-09-29"
draft: false
toc: false
comments: true
categories:
- News
tags:
- Biographers Guide
- Layout
---

I've spent the past month fighting with layout decisions. Steven and James have been great helps in ironing out kinks and suggesting ideas, but I'm reached a tipping point where I could use more help.

<!--more-->

The primary issue is laying out NPCs on the page. Since the entries now take nearly half of a page, quartering will no longer work. I have come up with a new option, however.

Vertical Layout:
<span class="npcimg"> ![Vertical](/screenshots/vertical_npcs.png)</span>
Horizontal Layout:
<span class="npcimg"> ![Horizontal](/screenshots/horizontal_npcs.png)</span>

The benefit to the banded horizontal layout is it smooths out the inconsistencies in length between each npc. While Bongo and Whumpchop happen to be near the same length, it does make it near impossible to insert artwork on a page with two NPCs. The solution might be to center and enlarge the artwork and have one NPC per page (in addition to racial banners).

Thoughts are welcome.

