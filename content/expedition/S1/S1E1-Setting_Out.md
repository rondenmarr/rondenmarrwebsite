---
title: "S1E1 - Setting Out"
date: 2019-04-15
draft: true
toc: false
comments: true
type: episode
Season: S1
Episode: E1
duration: 30:14
audio:
  - format: audio/mpeg
    filename: Expedition_of_Abbot_Shaw_S1E1_-_Setting_Out.mp3
    bytelength: 12322
categories:
- Expedition
---
Abbot introduces himself and begins his journey.

