---
title: "Press Kit"
date: 2019-04-25T16:58:44-04:00
draft: false
toc: false
type: page
comments: false
categories:
- Expedition
tags:
- podcast
---
# Overview
**The Expedition of Abbot Shaw** is a fantasy/horror audiodrama created by Jesse Morgan. ***Expedition*** is set in the world of *Ronden Marr* and follows the journey of **Abbot Shaw**, a sorcerer and explorer, into the unknown depths of *The Undercavern.*

<!--more-->

# Team
**JESSE MORGAN** Producer, Writer

Jesse is a systems engineer, developer, gamer, and podcast lover. His podcast feed consists of over 100 podcasts, which inspired him to create this podcast.

# Contact

Website: https://rondenmarr.com/expedition/

Email: RondenMarr@gmail.com

Twitter: @RondenMarr

Facebook https://www.facebook.com/RondenMarr/

# Where to Find It

RSS Feed: <a href="/expedition/index.xml" type="application/rss+xml" title="Podcast RSS"><i class="fa fa-rss-square fa-lg"></i></a>

# Release Schedule
**The Expedition of Abbot Shaw** is scheduled to be released every two weeks.
