---
title: The Expedition of Abbot Shaw
type: page
draft: false
toc: false
comments: false
---
Welcome to the *Expedition of Abbot Shaw*!

This is the home of the serialized podcast following the story of an explorer named Abbot Shaw as he delves into The Undercavern.

Follow Shaw and his familiar Briar as they leave the relative safety of the underground kingdom of Ronden Marr to delve deeper into the labyrinth of uncharted caves below. Danger and death lie around every corner, but it's rarely ever what you'd expect.


[Cast](/expedition/cast)

[Press Kit](/expedition/presskit/)

We are currently in pre-production, so keep an eye out for future updates!

**Content Advisory:** The *Expedition of Abbot Shaw* will generally follow a PG-13/Teen rating due to fictionalized violence, mild horror, and intense scenes. Viewer descretion is advised.
