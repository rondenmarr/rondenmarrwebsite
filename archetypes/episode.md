---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
type: episode
date: {{ .Date }}
draft: true
toc: false
comments: false
series: episode
categories:
- category1
- category2
tags:
- tag1
- tag2
---


<!--more-->
